#ifndef Timer_hpp
#define Timer_hpp

#include <cstdint>

class Timer
{
    public:
		//Initializes variables
		Timer();

		//The various clock actions
		void start();
		void stop();
		void pause();
		void unpause();

		//Gets the timer's time
		uint32_t getTicks();

		//Checks the status of the timer
		inline bool isStarted() { return started; }
		inline bool isPaused() { return paused && started; }

    private:
		//The clock time when the timer started
		uint32_t startTicks;

		//The ticks stored when the timer was paused
		uint32_t pausedTicks;

		//The timer status
		bool paused;
		bool started;
};

#endif
