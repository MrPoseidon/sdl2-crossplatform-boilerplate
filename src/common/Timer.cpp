#include "Timer.hpp"
#include <SDL2/SDL.h>

Timer::Timer() 
{
    this->stop();
}

void Timer::start()
{
    started = true;
    paused = false;

    startTicks = SDL_GetTicks();
    pausedTicks = 0;
}

void Timer::stop() 
{
    startTicks = pausedTicks = 0;
    paused = false;
    started = false;
}

void Timer::pause() 
{
    if(started && !paused) {
        paused = true;

        pausedTicks = SDL_GetTicks() - startTicks;
        startTicks = 0;
    }
}

void Timer::unpause() {
    if (started && paused) {
        paused = false;

        startTicks = SDL_GetTicks() - pausedTicks;
        pausedTicks = 0;
    }
}

uint32_t Timer::getTicks() {
    uint32_t time = 0;

    if (started) {
        if(paused) {
            time = pausedTicks;
        } else {
            time = SDL_GetTicks() - startTicks;
        }
    }

    return time;
}
