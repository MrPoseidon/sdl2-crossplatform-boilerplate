#include "Game.hpp"
#include <iostream>

Game::Game() : running(0) {}

Game::~Game()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void Game::start()
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        std::cout << "SDL_Init ERROR:" << std::endl;
        return;
    }

    window = SDL_CreateWindow("Title", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == nullptr)
    {
        std::cout << "SDL_CreateWindow ERROR: " << std::endl;
        return;
    }

    renderer = SDL_CreateRenderer(window, -1, 0);
    if (renderer == nullptr)
    {
        std::cout << "SDL_CreateRenderer ERROR: " << std::endl;
        return;
    }

    this->running = 1;

    bg.init();
    debugLabel.init();

    while (this->running)
    {
        capTimer.start();
        handleEvents();
        update();
        render();

        int frameTicks = capTimer.getTicks();
        if (frameTicks < SCREEN_TICK_PER_FRAME)
        {
            SDL_Delay(SCREEN_TICK_PER_FRAME - frameTicks);
        }
    }
}

void Game::stop()
{
    this->running = 0;
}

void Game::onQuit()
{
    stop();
}

void Game::onClick()
{
    bg.setRandom();
}

void Game::handleEvents()
{
    SDL_Event evt;
    while (SDL_PollEvent(&evt))
    {
        if (evt.type == SDL_QUIT)
        {
            onQuit();
        }
        if (evt.type == SDL_MOUSEBUTTONDOWN)
        {
            onClick();
        }
    }
}

void Game::update()
{
    debugLabel.update();
}

void Game::render()
{
    SDL_RenderClear(renderer);
    SDL_SetRenderDrawColor(renderer, bg.red, bg.green, bg.blue, 255);
    debugLabel.render(renderer);
    SDL_RenderPresent(renderer);
}
