#ifndef Background_hpp
#define Background_hpp

class Background
{
public:
    int red;
    int green;
    int blue;
    void setRandom();
    void init();
    Background();
};

#endif
