#include "DebugLabel.hpp"

DebugLabel::DebugLabel()
{
    if (TTF_Init() == -1)
    {
        std::cout << "TTF_Init Error: " << TTF_GetError() << std::endl;
    }
}

DebugLabel::~DebugLabel()
{
    TTF_Quit();
    TTF_CloseFont(font);
}

void DebugLabel::init()
{
    countedFrames = 0;
    fpsTimer.start();

    font = TTF_OpenFont("assets/fonts/FreeSerif.ttf", 25);
    if (font == NULL)
    {
        std::cout << "TTF_OpenFont ERROR: " << TTF_GetError() << std::endl;
    }
}

void DebugLabel::update()
{
    float avgFPS = countedFrames / (fpsTimer.getTicks() / 1000.f);
    if (fpsTimer.getTicks() > 1000)
    {
        fpsTimer.start();
        countedFrames = 0;
    }

    fpsText.str("");
    fpsText << std::fixed << std::setprecision(1) << avgFPS;
    countedFrames += 1;
}

void DebugLabel::render(SDL_Renderer *renderer)
{
    SDL_Color black = {0, 0, 0, 255};
    SDL_Surface *txt = TTF_RenderText_Solid(font, fpsText.str().c_str(), black);
    SDL_Texture *debugLabel = SDL_CreateTextureFromSurface(renderer, txt);

    int texW = 0;
    int texH = 0;

    SDL_QueryTexture(debugLabel, NULL, NULL, &texW, &texH);
    SDL_Rect Message_rect = {5, 0, texW, texH};
    // create a rect

    SDL_RenderCopy(renderer, debugLabel, NULL, &Message_rect);

    SDL_DestroyTexture(debugLabel);
    SDL_FreeSurface(txt);
}
