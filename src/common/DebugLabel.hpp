#ifndef DebugLabel_hpp
#define DebugLabel_hpp

#include "Timer.hpp"
#include <SDL2/SDL_ttf.h>
#include <sstream>
#include <iostream>
#include <iomanip>

class DebugLabel
{
private:
    Timer fpsTimer;
    int countedFrames;
    std::stringstream fpsText;
    TTF_Font *font;

public:
    DebugLabel();
    ~DebugLabel();
    void init();
    void update();
    void render(SDL_Renderer *renderer);
};

#endif
