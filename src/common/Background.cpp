#include "Background.hpp"
#include <cstdlib>

Background::Background()
{
    setRandom();
}

void Background::init()
{
    setRandom();
}

void Background::setRandom()
{
    this->red = rand() % 255;
    this->green = rand() % 255;
    this->blue = rand() % 255;
}
