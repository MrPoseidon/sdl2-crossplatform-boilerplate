#ifndef Game_hpp
#define Game_hpp

#include <SDL2/SDL.h>
#include <ctime>
#include "Background.hpp"
#include "Timer.hpp"
#include "DebugLabel.hpp"

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int FRAMES_PER_SECOND = 60;
const int SCREEN_TICK_PER_FRAME = 1000/FRAMES_PER_SECOND;

class Game
{
private:
    int running;

    SDL_Window *window;
    SDL_Renderer *renderer;

    Background bg;
    
    Timer capTimer;

    DebugLabel debugLabel; 

public:
    Game();
    ~Game();

    void start();
    void stop();

    void handleEvents();
    void update();
    void render();

    inline bool isRunning() { return running; }

    void onQuit();
    void onClick();
};

#endif
